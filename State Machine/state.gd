extends Node

var state_machine

signal finished(next_state)

func _ready():
	pass

func setMachine(_machine):
	state_machine = _machine
	var _err = connect("finished",state_machine,"change")

func update(_delta):
	pass

func handleInput(_event):
	pass

func enterState(_from, _vector = -1):
	pass