extends "state.gd"

func update(delta):
	if !state_machine.actor.engine_flare.visible:
		state_machine.actor.engine_flare.visible = true
		if SceneHandler.sound_on:
			state_machine.actor.thruster_sound.play()
	state_machine.actor.camera.enableShake(0.2, 5.0)
	var direction = getInputDirection()
	direction = direction.rotated(state_machine.actor.rotation)
	if not direction:
		state_machine.actor.engine_flare.visible = false
		state_machine.actor.thruster_sound.stop()
		emit_signal("finished", "Idle")
	state_machine.move_vect = direction * delta * state_machine.speed
	state_machine.actor.move_and_collide(direction * delta * state_machine.speed)
	state_machine.actor.map.addEngineTrailPoint(state_machine.actor, state_machine.actor.engine_trail.global_position)

func handleInput(_event):
	if _event.is_action_pressed("jump_prep") && state_machine.aim_reset:
		state_machine.actor.engine_flare.visible = false
		state_machine.actor.thruster_sound.stop()
		emit_signal("finished", "Aim")

func getInputDirection():
	var input_direction = Vector2()
	input_direction.x = int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left"))
	input_direction.y = int(Input.is_action_pressed("move_down")) - int(Input.is_action_pressed("move_up"))
	return input_direction