extends "state.gd"

func handleInput(_event):
	state_machine.move_vect = Vector2.ZERO
	var direction = getInputDirection()
	if _event.is_action_pressed("jump_prep") && state_machine.aim_reset:
		emit_signal("finished", "Aim")
	if direction:
		emit_signal("finished", "Move")

func getInputDirection():
	var input_direction = Vector2()
	input_direction.x = int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left"))
	input_direction.y = int(Input.is_action_pressed("move_down")) - int(Input.is_action_pressed("move_up"))
	return input_direction