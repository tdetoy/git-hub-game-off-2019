extends Node

var states = []

var actor setget setEnemy, getEnemy

const speed = 100
const forward = Vector2(0, -1)

onready var state_map = {
	"Inbound":$Inbound,
	"Hunt":$Hunt,
	"Outbound":$Outbound,
	"Dying":$Dying,
	"Idle":$Idle
}

func _ready():
	for state in state_map.values():
		print(state.name)
		state.setMachine(self)

func setEnemy(enemy):
	actor = enemy

func getEnemy():
	return actor

func change(state):
	if state == "Dying" || state == "Outbound":
		state_map[state].enterState(null)
	elif state == "Inbound":
		state_map[state].enterState(actor.color, actor.initial_pos)
	if state == "return":
		var _prev_state = states.pop_front()
		return
	states.push_front(state)

func update(delta):
	state_map[states.front()].update(delta)