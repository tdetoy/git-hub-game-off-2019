extends "state.gd"

var hunt_time = 30.0

var increment = 0

func update(delta):
	hunt_time -= delta
	state_machine.actor.moveSelf(state_machine.forward.rotated(state_machine.actor.rotation) * state_machine.speed * delta)
	state_machine.actor.turret_group.turretBehavior(delta, state_machine.actor.map.player)
	state_machine.actor.map.addEngineTrailPoint(state_machine.actor, state_machine.actor.engine_trail.global_position)
	increment += 1
	if hunt_time <= 0.0:
		state_machine.change("Outbound")

