extends Node

var straight_vect = Vector2(0,-250)
var angle_max = PI / 3
var aim_reset = false

var states = []
var actor setget setPlayer,getPlayer
var move_vect = Vector2.ZERO

var store_vec = PoolVector2Array() setget setLineVec, getLineVec
var rotate_target = 0 setget setRotateTarget, getRotateTarget

const rot_max = 3.5
const speed = 2500
const jump_recharge = 0.15

onready var state_map = {
	"Idle":$Idle,
	"Move":$Move,
	"Aim":$Aim,
	"Travel":$Travel,
	"Rotate":$Rotate,
	"Dying":$Dying
}

func _ready():
	for state in state_map.values():
		print(state.name)
		state.setMachine(self)
	

func setPlayer(incoming):
	actor = incoming

func getPlayer():
	return actor

func change(state):
	if state == "return":
		var prev_state = states.pop_front()
		actor.label.text = states.front()
		if states.front() == "Travel":
			state_map[states.front()].enterState(prev_state)
		return
	if state == "Travel" || state == "Aim" || state == "Dying":
		state_map[state].enterState(states.front(),store_vec)
	states.push_front(state)
	actor.label.text = states.front()

func update(delta):
	if !Input.is_action_pressed("jump_prep"):
		aim_reset = true
	var line = PoolVector2Array()
	line.append(Vector2(0,0))
	
	#woof, ok
	#get mouse position, translate to local coordinates for the player scene
	#make sure it's only a certain magnitude regardless of mouse position
	#and finally make sure it lies within a certain angle left or right of the front of the ship
	var rot_speed
	line.append(clampVector(normalizeSteeringVector(actor.to_local(actor.camera.get_global_mouse_position()), 250), angle_max))

	rot_speed = range_lerp(determineAngle(line[1]), 0 - angle_max, angle_max, 0 - rot_max, rot_max)
	actor.ship.rotation = range_lerp(determineAngle(line[1]), 0 - angle_max, angle_max, 0 - (3 * PI / 7), (3 * PI / 7))
	actor.coll_poly.rotation = range_lerp(determineAngle(line[1]), 0 - angle_max, angle_max, 0 - (3 * PI / 7), (3 * PI / 7))
	actor.line.points = line
	actor.rotation += (rot_speed * delta)
	if states.front() != "Aim":
		actor.map._UI.jump_meter.value += jump_recharge
	state_map[states.front()].update(delta)

func handleInput(event):
	state_map[states.front()].handleInput(event)

func getLineVec():
	return store_vec

func setRotateTarget(value):
	rotate_target = value

func getRotateTarget():
	return rotate_target

func determineAngle(vector):
	var angle = vector.angle_to(straight_vect)
	angle *= -1
	return angle

func clampVector(vector, angle_clamp):
	var angle = vector.angle_to(straight_vect)
	var magnitude = sqrt(vector.x * vector.x + vector.y * vector.y)
	if angle > 0:
		return straight_vect.rotated(0 - min(angle, angle_clamp)) * magnitude
	elif angle < 0:
		return straight_vect.rotated(0 - max(angle, 0 - angle_clamp)) * magnitude
	return vector

func normalizeSteeringVector(vector, magnitude):
	var return_val = vector.normalized()
	return_val *= magnitude
	return return_val

func setLineVec(line_vec):
	store_vec = line_vec

func determineHeading(pos):
	return pos.x - (actor.camera.get_viewport().size.x / 2)