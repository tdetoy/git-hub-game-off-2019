extends "state.gd"

var direction_vec = null

func update(_delta):
	var vector = direction_vec
	vector = vector.rotated(state_machine.actor.rotation)
	var ghost_positions = randPoints(vector)
	for ghost_pos in ghost_positions:
		state_machine.actor.placeGhost(ghost_pos)
	state_machine.actor.position += vector
	yield(get_tree(), "physics_frame")
	var areas = state_machine.actor.jump_wake.get_overlapping_areas()
	if areas != null:
		for area in areas:
			print(area.name)
			if area.is_in_group("Reactor"):
				area.get_parent().die()
#	var result = state_machine.actor.move_and_collide(Vector2.ZERO)
#	if result != null:
#		if result.collider.is_in_group("Enemy"):
#			state_machine.actor.damage(50)
#		print(result.collider.name)
	emit_signal("finished", "Idle")

func randPoints(in_vect):
	var return_points = []
	var magnitude = 1250
	var normalized = in_vect.normalized()
	var index = 0
	randomize()
	while index < 5:
		var rand_point = normalized * (randi() % magnitude)
		return_points.append(rand_point)
		index += 1
	return return_points

func enterState(from, vector = null):
	state_machine.actor.sound_player.stream = load("res://World/Sounds/330402__assimulation-gaming__hyperdrive1.wav")
	if SceneHandler.sound_on:
		state_machine.actor.sound_player.play()
	state_machine.actor.map.resetPlayerTrail(state_machine.actor)
	direction_vec = vector

func handleInput(event):
	pass