extends "state.gd"

func enterState(_from, _vector = null):
	state_machine.actor.setupTween(state_machine.rotate_target)

func _on_Tween_tween_all_completed():
	emit_signal("finished", "return")
