extends "state.gd"

var jump_warning = load("res://Objects/JumpWarning/JumpWarning.tscn")
var warp_ghost = load("res://Objects/WarpGhost/WarpGhost.tscn")

var jump_warn

const warning_time = 6.0
const max_ghosts = 10
const min_ghosts = 5

var ghost_num
var remaining_time

func update(_delta):
	remaining_time -= _delta
	if remaining_time <= 0.3:
		state_machine.actor.visible = true
		while ghost_num > 0:
			var ghost = warp_ghost.instance()
			state_machine.actor.getSpawnPoint().add_child(ghost)
			ghost.position = state_machine.actor.initial_pos - (state_machine.actor.initial_facing * rand_range(-4000, -2000))
			ghost.rotation = state_machine.actor.initial_rotation
			ghost.initialize("Enemy")
			ghost_num -= 1
#		var debug_preview = state_machine.actor.initial_pos - (state_machine.actor.initial_facing * -100)
		var offset_mult = range_lerp(remaining_time, 0.3, 0.0, -100, 0)
		state_machine.actor.position = state_machine.actor.initial_pos - (state_machine.actor.initial_facing * offset_mult)
		state_machine.actor.rotation = state_machine.actor.initial_rotation
	if remaining_time <= 0.0:
		if SceneHandler.sound_on:
			state_machine.actor.warp_noise.play()
		jump_warn.queue_free()
#		if state_machine.actor.coll_poly.
		state_machine.change("Hunt")

func enterState(_from, _vector = null):
	randomize()
	jump_warn = jump_warning.instance()
	state_machine.actor.getSpawnPoint().add_child(jump_warn)
	jump_warn.position = _vector
	jump_warn.rotation = state_machine.actor.initial_rotation
	jump_warn.colorModulate(_from)
	remaining_time = warning_time
	ghost_num = (randi() % (max_ghosts - min_ghosts)) + min_ghosts