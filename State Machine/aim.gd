extends "state.gd"

const jump_angle_max = PI / 4
const zero_points = PoolVector2Array([Vector2.ZERO, Vector2.ZERO])
const jump_deplete = 0.2

onready var tween_in = $TweenIn
onready var tween_out = $TweenOut

func update(_delta):
	state_machine.actor.map._UI.jump_meter.value -= jump_deplete
	if state_machine.actor.map._UI.jump_meter.value <= 0.0:
		if state_machine.actor.sound_player.is_playing():
			state_machine.actor.sound_player.stop()
		if tween_in.is_active():
			tween_in.remove_all()
		tween_out.interpolate_method(Engine, "set_time_scale", Engine.get_time_scale(), 1.0, 0.1, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		tween_out.start()
		state_machine.actor.jump_line.set_points(zero_points)
		emit_signal("finished","Idle")
		return
	var direction = getInputDirection()
	direction = direction.rotated(state_machine.actor.rotation)
	var jump_points = PoolVector2Array()
	jump_points.append(Vector2(0,0))
	jump_points.append(state_machine.clampVector(state_machine.normalizeSteeringVector(state_machine.actor.to_local(state_machine.actor.camera.get_global_mouse_position()), 5), jump_angle_max))
	state_machine.actor.ship.rotation = state_machine.determineAngle(jump_points[1])
	state_machine.actor.jump_line.set_points(jump_points)
	
	if Input.is_action_just_released("jump_prep"):
		if state_machine.actor.sound_player.is_playing():
			state_machine.actor.sound_player.stop()
		if tween_in.is_active():
			tween_in.remove_all()
		tween_out.interpolate_method(Engine, "set_time_scale", Engine.get_time_scale(), 1.0, 0.1, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		tween_out.start()
		state_machine.actor.jump_line.set_points(zero_points)
		emit_signal("finished","Idle")
	elif Input.is_action_just_pressed("jump_init"):
		if state_machine.actor.sound_player.is_playing():
			state_machine.actor.sound_player.stop()
		state_machine.actor.map._UI.jump_meter.value -= 30
		if tween_in.is_active():
			tween_in.remove_all()
		tween_out.interpolate_method(Engine, "set_time_scale", Engine.get_time_scale(), 1.0, 0.1, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		tween_out.start()
		state_machine.store_vec = jump_points[1]
		state_machine.actor.jump_line.set_points(zero_points)
		emit_signal("finished", "Travel")
	state_machine.actor.move_and_collide(direction * _delta * state_machine.speed)

func getInputDirection():
	var input_direction = Vector2()
	input_direction.x = int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left"))
	input_direction.y = int(Input.is_action_pressed("move_down")) - int(Input.is_action_pressed("move_up"))
	return input_direction

func enterState(_from, _vector = null):
	state_machine.actor.sound_player.stream = load("res://World/Sounds/257229__javierzumer__charging-power.wav")
	if SceneHandler.sound_on:
		state_machine.actor.sound_player.play()
	state_machine.aim_reset = false
	tween_in.interpolate_method(Engine, "set_time_scale", 1.0, 0.1, 0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tween_in.start()