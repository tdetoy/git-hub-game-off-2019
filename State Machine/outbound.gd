extends "state.gd"

var warp_ghost = load("res://Objects/WarpGhost/WarpGhost.tscn")

const charge_time = 10.0
const max_ghosts = 10
const min_ghosts = 5

var time_left
var ghost_num
#var remaining_time

func enterState(_from, _vector = null):
	state_machine.actor.get_node("Particles2D").emitting = true
	time_left = charge_time
	randomize()
	ghost_num = (randi() % (max_ghosts - min_ghosts)) + min_ghosts

func update(_delta):
	time_left -= _delta
	if time_left <= 0.1:
		state_machine.actor.visible = false
		while ghost_num > 0:
			var ghost = warp_ghost.instance()
			state_machine.actor.getSpawnPoint().add_child(ghost)
			ghost.position = state_machine.actor.initial_pos - (state_machine.actor.initial_facing * rand_range(4000, 2000))
			ghost.rotation = state_machine.actor.initial_rotation
			ghost.initialize("Enemy")
			ghost_num -= 1
		var offset_mult = range_lerp(time_left, 0.1, 0.0, 100, 0)
		state_machine.actor.position = state_machine.actor.initial_pos - (state_machine.actor.initial_facing * offset_mult)
		if SceneHandler.sound_on:
			state_machine.actor.warp_noise.play()
		state_machine.actor.warpOut()