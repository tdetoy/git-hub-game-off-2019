extends "state.gd"

var explosion = load("res://Objects/Explosion/Explosions.tscn")

var explosion_number
var explosion_countdown
var explosion_extents
var final_countdown = 1.5
var finale = false
var rot_val

const explosion_max = 10
const explosion_min = 5

func enterState(_from, _vector = null):
	randomize()
	rot_val = rand_range(-.1, .1)
	explosion_number = randi() % (explosion_max - explosion_min) + explosion_min
	explosion_countdown = determineNextExplosion()
	explosion_extents = determineExtents(state_machine.actor.coll_poly)

func update(_delta):
	explosion_countdown -= _delta
	if explosion_countdown <= 0.0 and explosion_number > 0:
		explosion_countdown = determineNextExplosion()
		var explode = explosion.instance()
		state_machine.actor.placeMapExplosion(explode)
		explode.position.x += rand_range(explosion_extents[1].x, explosion_extents[0].x)
		explode.position.y += rand_range(explosion_extents[1].y, explosion_extents[0].y)
		explode.emitting = true
		explosion_number -= 1
		print(explosion_number)
	if explosion_number == 0 and !finale:
		var big_explode = explosion.instance()
		state_machine.actor.add_child(big_explode)
		big_explode.bigBoom(state_machine.actor, explosion_extents)
		finale = true
	if finale:
		final_countdown -= _delta
#	if final_countdown <= 0.0:
#		state_machine.actor.deathComplete()
	state_machine.actor.move_and_collide(state_machine.move_vect)
	state_machine.actor.ship.rotation += rot_val

func determineNextExplosion():
	return rand_range(0.01 * explosion_number / 5, 0.1 * explosion_number / 5)

func determineExtents(coll_poly):
	var highest_extents = Vector2.INF
	var lowest_extents = Vector2.INF
	for point in coll_poly.polygon:
		if highest_extents == Vector2.INF:
			highest_extents = point
		if lowest_extents == Vector2.INF:
			lowest_extents = point
		highest_extents.x = max(point.x, highest_extents.x)
		highest_extents.y = max(point.y, highest_extents.y)
		lowest_extents.x = min(point.x, lowest_extents.x)
		lowest_extents.y = min(point.y, lowest_extents.y)
	print(highest_extents)
	print(lowest_extents)
	return [highest_extents, lowest_extents]