extends Particles2D

signal disappear
signal done

onready var sprite = $Sprite

var big_size_x = 0.0
var big_size_y = 0.0

var life = 2.0
var explosion_core
var stage_length
var current_length
var stage = 0
var transitioned = false
var big = false
var disappeared = false

func _ready():
	randomize()
	explosion_core = rand_range(0.3, 0.4)
	stage_length = explosion_core / 6
	current_length = stage_length
#	emitting = true
	set_process(true)

func bigBoom(actor, extents):
	#big if true
	big = true
	life = 2.5
	connect("disappear", actor, "hide")
	connect("done", actor, "deathComplete")
	var extents_rect = Rect2(extents[0], extents[0] - extents[1])
	big_size_x = (extents_rect.size.x / $Sprite.get_rect().size.x) * 2.5
	big_size_y = (extents_rect.size.y / $Sprite.get_rect().size.y) * 2.5

func _process(delta):
	life -= delta
	if !big:
		if SceneHandler.sound_on:
			$LittleBoom.play()
		current_length -= delta
		if current_length <= 0.0 and transitioned:
			stage += 1
			transitioned = false
		if !transitioned:
			match stage:
				0:
					sprite.scale = Vector2(0.5, 0.5)
				1:
					sprite.scale = Vector2(1.0,1.0)
				2:
					sprite.frame = 1
				3:
					sprite.frame = 2
				4:
					sprite.frame = 3
				5:
					sprite.frame = 4
				6:
					sprite.set_visible(false)
			transitioned = true
	if big:
		if !$BigBoom.is_playing() and SceneHandler.sound_on:
			$BigBoom.play()
		sprite.scale.x = range_lerp(life, 2.5, 0.0, 0.1, big_size_x) 
		sprite.scale.y = range_lerp(life, 2.5, 0.0, 0.1, big_size_y) 
		sprite.modulate.a = range_lerp(life, .75, 0.0, 1.0, 0.0)
		sprite.modulate.b = range_lerp(life, .75, 0.0, 1.0, 0.0)
		if life <= .75 and !disappeared:
			emit_signal("disappear")
			disappeared = true
	if life <= 0.0:
		emit_signal("done")
		queue_free()