extends KinematicBody2D

signal dead(actor)

const max_health = 100.0

onready var state_machine = $StateMachine
onready var label = $Misc/Label setget ,getLabel
onready var line = $Misc/Line2D setget ,getLine
onready var camera = $Camera2D setget ,getCamera
onready var tween = $Misc/Tween setget ,getTween
onready var ship = $Sprite setget ,getSprite
onready var jump_line = $Misc/JumpLine setget ,getJumpLine
onready var jump_wake = $Misc/Area2D setget ,getJumpWake
onready var coll_poly = $CollisionShape2D
onready var engine_trail = $Sprite/EngineTrail
onready var engine_flare = $Sprite/EngineTrail/EngineFlare
onready var warning_player = $Misc/Warning
onready var sound_player = $Misc/Generic
onready var damage_sound = $Misc/Damage
onready var thruster_sound = $Misc/Thruster

var warp_ghost = load("res://Objects/WarpGhost/WarpGhost.tscn")
var hint_path = load("res://Objects/HintArrow/HintArrow.tscn")

var death_bit = false
var hint_arrows = {}
var current_health
var oob_damage_mult = 1.0
var executed = false

var map = null

func _ready():
	current_health = max_health
	state_machine.setPlayer(self)
	state_machine.change("Idle")
	set_physics_process(true)

func _physics_process(delta):
	if (position.x > 30000 || position.x < -30000 || position.y > 30000 || position.y < -30000) && state_machine.states.front() != "Dying":
		map._UI.activateOOB()
		damage(.5 * oob_damage_mult * delta)
		oob_damage_mult += 1.5
	elif map._UI.OOBActivated():
		oob_damage_mult = 1.0
		map._UI.deactivateOOB()
	iterateArrows(map.getEnemyList())
	state_machine.update(delta)
	if death_bit and !executed:
		visible = false
		executed = true
		emit_signal("dead", self)

func _unhandled_input(event):
	state_machine.handleInput(event)

func getLabel():
	return label

func getLine():
	return line

func getCamera():
	return camera

func getTween():
	return tween

func setupTween(target_rot):
		tween.interpolate_property(self, "rotation_degrees", rotation_degrees, rad2deg(target_rot), 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		tween.start()

func attachMap(map_in):
	map = map_in
	map.registerEngineTrail(self, Color(0.5, 0.8, 0.82))
	connect("dead", map, "onEntityDeath")

func getSprite():
	return ship

func getJumpLine():
	return jump_line

func damage(value):
	camera.enableShake(.5, 1000.0)
	current_health -= value
	map._UI.updateHealth(current_health/max_health)
	if SceneHandler.sound_on:
		damage_sound.play()
	if current_health <= 0.0:
		die()

func die():
	state_machine.change("Dying")

func getJumpWake():
	return jump_wake

func placeGhost(in_position = Vector2.INF):
	var ghost_instance = warp_ghost.instance()
	get_parent().add_child(ghost_instance)
	if in_position == Vector2.INF:
		ghost_instance.global_position = global_position
	else:
		ghost_instance.position = in_position + position
	ghost_instance.initialize("Player")
	ghost_instance.rotation = rotation

func iterateArrows(enemy_list):
	if enemy_list.size() == 0 and hint_arrows.size() == 0:
		return
	elif enemy_list.size() == 0:
		hint_arrows.clear()
	for arrow in hint_arrows.keys():
		if enemy_list.has(arrow):
			if !enemy_list[arrow].get_ref():
				hint_arrows[arrow].queue_free()
				hint_arrows.erase(arrow)
		else:
			hint_arrows[arrow].queue_free()
			hint_arrows.erase(arrow)
	for enemy in enemy_list.values():
		if enemy.get_ref():
			var index = enemy.get_ref()
			if !hint_arrows.has(index.name):
				hint_arrows[index.name] = hint_path.instance()
				add_child(hint_arrows.get(index.name))
				hint_arrows[index.name].visible = false
			var difference_vect = -position + index.position
			hint_arrows.get(index.name).position = Vector2(0, 1).rotated((Vector2(0, 1).rotated(rotation)).angle_to(difference_vect))#.rotated(rotation)))
			hint_arrows.get(index.name).position *= 1000
			hint_arrows.get(index.name).rotation = Vector2(0, 1).angle_to(hint_arrows.get(index.name).position) - PI
			if index.getState() == "Hunt":
				if !hint_arrows.get(index.name).is_visible():
					hint_arrows.get(index.name).visible = true
				hint_arrows.get(index.name).showDistance(index.state_machine.state_map["Hunt"].hunt_time)

func placeMapExplosion(explode):
	get_parent().getExplosionParent().add_child(explode)
	explode.global_position = global_position
	explode.rotation = rotation

func deathComplete():
	death_bit = true

func lowHealth():
	if SceneHandler.sound_on:
		warning_player.play()

func _on_OverlapShape_body_entered(body):
	if body.is_in_group("Enemy"):
		if body.getState() == "Hunt":
			damage(50)
		elif body.getState() == "Inbound":
			damage(100)
