extends Camera2D

export var rot_deadzone = 200

const default_pos = Vector2(0, -560)

var lerp_mult = 100
var lerp_ind = 0.0

var state = null

var duration = 0.0
var intensity = 0.0
var remaining = 0.0

enum STATES{normal, shake, lag}

func _ready():
	set_physics_process(true)

func _physics_process(delta):
	match state:
		STATES.normal:
			pass
		STATES.shake:
			processShake(delta)
		STATES.lag:
			pass

func enableShake(shake_duration, shake_intensity):
	if duration < shake_duration || intensity < shake_intensity:
		duration = shake_duration
		remaining = shake_duration
		intensity = shake_intensity
		state = STATES.shake

func processShake(delta):
	remaining -= delta
	var mult = range_lerp(remaining, duration, 0.0, intensity, 0.0)
	var new_pos = Vector2()
	new_pos.x = default_pos.x + (mult * rand_range(-1.0, 1.0))
	new_pos.y = default_pos.y + (mult * rand_range(-1.0, 1.0))
	position = new_pos
	if remaining <= 0.0:
		position = default_pos
		duration = 0.0
		remaining = 0.0
		intensity = 0.0
		state = STATES.normal
