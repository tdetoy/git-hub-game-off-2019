extends Node2D

func showDistance(distance):
	$Sprite.modulate.r = range_lerp(distance, 30.0, 0.0, 0.0, 1.0)
	$Sprite.modulate.g = range_lerp(distance, 30.0, 0.0, 1.0, 0.0)
	$Sprite.modulate.a = range_lerp(distance, 30.0, 0.0, 0.5, 1.0)