extends Sprite

var player_sprite = load("res://Objects/Player/player_ship.model.png")
var enemy_sprite = load("res://Objects/Enemy/frigate.png")

var initialized = false

func _ready():
	$Tween.interpolate_property(self, "modulate", Color(1.0,1.0,1.0,1.0), Color(1.0,1.0,1.0,0.0), 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	set_process(true)

func initialize(model):
	if model == "Player":
		texture = player_sprite
	elif model == "Enemy":
		texture = enemy_sprite
		scale = Vector2(5, 5)
	initialized = true

func _process(_delta):
	if initialized:
		$Tween.start()

func _on_Tween_tween_all_completed():
	queue_free()
