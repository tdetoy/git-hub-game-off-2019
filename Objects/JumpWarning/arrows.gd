extends Node2D

onready var arrows = [$Arrow1, $Arrow2, $Arrow3]

const cycle_speed = 0.5

var curr_cycle = 0.0
var stage = 0
var change = false

func _ready():
	curr_cycle = cycle_speed
	set_process(true)

func _process(delta):
	curr_cycle -= delta
	if curr_cycle <= 0.0:
		stage = (stage + 1) % 4
		curr_cycle = cycle_speed
		change = true
	if change:
		if stage == 3:
			for arrow in arrows:
				arrow.visible = false
		else:
			arrows[stage].visible = true
		change = false

func colorModulate(color):
	for arrow in arrows:
		arrow.modulate = color