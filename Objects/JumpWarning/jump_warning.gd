extends Node2D

func colorModulate(color):
	$Lines.colorModulate(color)
	$Arrows.colorModulate(color)
	$Text.colorModulate(color)
	$Arrows2.colorModulate(color)
	$Text2.colorModulate(color)
