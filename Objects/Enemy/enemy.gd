extends StaticBody2D

signal dead(actor)

onready var ship = $Sprites/Ship
onready var state_machine = $StateMachine
onready var reactor_collider = $Area2D/ReactorShape
onready var coll_poly = $EntireShip
onready var engine_trail = $TrailSpawn
onready var warp_noise = $AudioStreamPlayer2D

var turret_group = null setget setTurrets, getTurrets

var initial_pos = Vector2.INF setget , getInit
var initial_rotation = 0.0
var initial_facing = Vector2.INF setget , getInitFacing
var color = null setget setColor, getColor
var map

var death_bit = false

var _turret_small = preload("res://Objects/Enemy/TurretGroups/TurretGroupSmall.tscn")
var _turret_large = preload("res://Objects/Enemy/TurretGroups/TurretGroupLarge.tscn")

func _ready():
	state_machine.setEnemy(self)
	
	randomize()
	var group = randi() % 2#3
	var turret_instance = null
	match group:
		0:
			turret_instance = _turret_small.instance()
		1:
			turret_instance = _turret_large.instance()
#		2:
#			turret_instance = _turret_large.instance()
	turret_group = turret_instance
	add_child(turret_instance)
	turret_instance.attachParent(self)
	
	set_physics_process(true)

func _physics_process(delta):
	state_machine.update(delta)
	if death_bit:
		emit_signal("dead", self)
		queue_free()

func moveSelf(force):
	position += force

func setTurrets(group):
	turret_group = group

func getTurrets():
	return turret_group

func getBulletSpawn():
	return get_parent().getBulletParent()

func die():
	state_machine.change("Dying")

func deathComplete():
	death_bit = true

func hide():
	if !turret_group.is_queued_for_deletion():
		turret_group.queue_free()
	coll_poly.disabled = true
	reactor_collider.disabled = true
	ship.visible = false

func placeMapExplosion(explode):
	get_parent().getExplosionParent().add_child(explode)
	explode.position += position
	explode.rotation = rotation

func getSpawnPoint():
	return get_parent().getSpawnPoint()

func setInit(init_pos : Vector2, init_rot : float, store_pos : Vector2, in_map):
	position = store_pos
	initial_pos = init_pos
	initial_rotation = init_rot
	initial_facing = Vector2(0, 1)
	initial_facing = initial_facing.rotated(init_rot)
	initial_facing = initial_facing.normalized()
	map = in_map
	connect("dead", map, "onEntityDeath")
	state_machine.change("Inbound")

func getInit():
	if initial_pos == Vector2.INF:
		prints("Uninitialized position for", self.name)
		return Vector2.ZERO
	else:
		return initial_pos

func setColor(col : Color):
	color = col

func getColor():
	if color == null:
		prints("Unitialized color for", self.name)
		return Color.white
	else:
		return color

func getInitFacing():
	if initial_facing == Vector2.INF:
		prints("Unitialized facing for", self.name)
		return Vector2.ONE
	else:
		return initial_facing

func warpOut():
	map._UI.shipEscaped()
	queue_free()

func getState():
	return state_machine.states.front()