extends "turret_group.gd"


var laser_path = load("res://Objects/Enemy/TurretGroups/Turret/Laser/Laser.tscn")

var dummy_obj = Line2D.new()

var laser_instances = [weakref(dummy_obj), weakref(dummy_obj)]

func turretBehavior(_delta, _target = null):
	var idx = 0
	for turret in turrets:
		if !laser_instances[idx].get_ref() || laser_instances[idx].get_ref() == dummy_obj:
			laser_instances[idx] = weakref(laser_path.instance())
			turret.add_child(laser_instances[idx].get_ref())
			turret.get_node("RayCast2D").position = turret.getNextLocalBarrel()
			laser_instances[idx].get_ref().setLaser(true, self)
			laser_instances[idx].get_ref().setColor(actor.color)
#			laser_instances[idx].get_ref().position = turret.get_node("RayCast2D").position
			turret.get_node("RayCast2D").cast_to = Vector2(0, -3000)
			laser_instances[idx].get_ref().track_points = [turret.get_node("RayCast2D").position, turret.get_node("RayCast2D").position + turret.get_node("RayCast2D").cast_to]
		elif !laser_instances[idx].get_ref().isLocked():
			if _target != null && is_instance_valid(_target):
				turret.rotation = Vector2(0,1).angle_to(to_local(_target.global_position + turret.position)) - PI
		idx += 1

func laserFire(laser):
	var idx = 0
	var found = false
	for laser_ref in laser_instances:
		if laser_ref.get_ref() == laser:
			found = true
		if !found:
			idx += 1
	if !found:
		print("something went wrong - can't find laser!")
		return
	var turret_cast = turrets[idx].get_node("RayCast2D")
	if turret_cast == null:
		print("something went wrong - can't find turret cast!")
		return
	if turret_cast.is_colliding() && turret_cast.get_collider().is_in_group("Player"):
		turret_cast.get_collider().damage(10)