extends Area2D

var lifetime = 30

const speed = 1000
const forward = Vector2(0, -1)

func _ready():
	set_physics_process(true)

func _physics_process(delta):
	position += forward.rotated(rotation) * speed * delta
	lifetime -= delta
	if lifetime < 0:
		die()

func die():
	queue_free()

func _on_Bullet_body_entered(body):
	if body.is_in_group("Player"):
		body.damage(5)
	die()