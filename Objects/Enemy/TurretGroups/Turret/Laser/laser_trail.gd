extends Line2D

signal fire(laser)

onready var fire_sound = $Fire

#if true, laser
#if false, engine contrail
var is_laser = true

#laser variables
const warm_up = 2.0
const fire_time = 0.5
var time_left
var firing = false

#engine contrail variables
var trail_lifetime = 0.2
var point_lifetime = []
var max_size = 15

var track_points = [] setget setTrackPoints, getTrackPoints

func _ready():
	var _output = $Tween.connect("tween_all_completed", self, "tweenComplete")
	set_process(true)

func _process(delta):
	if is_laser:
		time_left -= delta
		if !firing && time_left < 0.2:
			if $Tracer.default_color == Color(1.0, 0.0, 0.0, 0.8):
				$Tracer.default_color = Color(1.0, 1.0, 1.0, 0.8)
			elif $Tracer.default_color == Color(1.0, 1.0, 1.0, 0.8):
				$Tracer.default_color = Color(1.0, 0.0, 0.0, 0.8)
		if time_left <= 0.0:
			if !firing:
				if SceneHandler.sound_on:
					fire_sound.play()
				emit_signal("fire", self)
				firing = true
				points = track_points
				$Particles2D.visible = true
				$Tracer.visible = false
				$Tween.interpolate_property(self, "modulate", Color(1.0,1.0,1.0,1.0), Color(1.0,1.0,1.0,0.0), fire_time, Tween.TRANS_LINEAR, Tween.EASE_OUT)
				$Tween.start()
	elif !is_laser:
		points = track_points
		if point_lifetime.size() > 0:
			if OS.get_system_time_msecs() - point_lifetime.back() > trail_lifetime * 1000:
				point_lifetime.pop_back()
				track_points.pop_back()

func isLocked():
	return time_left < 0.5 || firing

func setLaser(value, _creator = null):
	is_laser = value
	if is_laser:
		time_left = warm_up
		connect("fire", _creator, "laserFire")
	elif !is_laser:
		$Tracer.visible = false
		$Particles2D.visible = true
		width = 30.0
		z_index = -2

#for engine trail
func addTrackPoint(point):
	track_points.push_front(point)
	point_lifetime.push_front(OS.get_system_time_msecs())
	if track_points.size() > max_size:
		track_points.pop_back()
		point_lifetime.pop_back()

func getTrackPoints():
	return track_points

#strictly for laser
#takes start and end points and (ideally) inserts
#two additional points: one at 10% of the line and one at 95%
#(95% is so as much of the laser as possible is visible to avoid "phantom hits")
func setTrackPoints(track_in):
	var temp_track = []
	temp_track.append(track_in[0])
	temp_track.append(track_in[0] + (track_in[1].normalized() * sqrt(pow(track_in[1].x, 2) + pow(track_in[1].y, 2)) * .1))
	temp_track.append(track_in[0] + (track_in[1].normalized() * sqrt(pow(track_in[1].x, 2) + pow(track_in[1].y, 2)) * .95))
	temp_track.append(track_in[1])
	track_points = temp_track
	$Tracer.points = track_points

func setColor(color):
	var floats = [0, 0.1, 0.9, 1]
	var colors = [Color(1.0,1.0,1.0,1.0), color, color, Color(color.r,color.g,color.b,0.0)]
	var grad = Gradient.new()
	var index = 0
	while index < 4:
		grad.add_point(floats[index], colors[index])
		index += 1
	gradient = grad
	$Particles2D.process_material.color_ramp.gradient = grad

func tweenComplete():
	queue_free()

func clearPoints():
	track_points.clear()
	point_lifetime.clear()

#should really only need to be called to set a long trail lifetime
#i.e. "true"
func setLongTrail(value):
	if value:
		trail_lifetime = 5.0
		max_size = 50
		width = 300
	elif !value:
		trail_lifetime = 0.2