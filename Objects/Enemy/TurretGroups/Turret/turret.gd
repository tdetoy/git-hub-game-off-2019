extends Sprite

const forward_vect = Vector2(0,-1)

onready var left_barrel = $LeftBarrel setget ,getLeftBarrel
onready var right_barrel = $RightBarrel setget ,getRightBarrel

var left_barrel_turn = true
var resting_rotation

func _ready():
	resting_rotation = rotation

func getLeftBarrel():
	return left_barrel.global_position

func getRightBarrel():
	return right_barrel.global_position

func getRightLocalBarrel():
	return right_barrel.position

func getLeftLocalBarrel():
	return left_barrel.position

func getNextBarrel():
	if left_barrel_turn:
		left_barrel_turn = false
		return getLeftBarrel()
	else:
		left_barrel_turn = true
		return getRightBarrel()

func getNextLocalBarrel():
	if left_barrel_turn:
		left_barrel_turn = false
		return getLeftLocalBarrel()
	else:
		left_barrel_turn = true
		return getRightLocalBarrel()