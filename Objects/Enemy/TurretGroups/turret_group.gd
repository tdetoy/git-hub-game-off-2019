extends Node2D

onready var timer_time = $Timer setget setTime, getTime


var armed = false
var turrets = []
var actor = null

func _ready():
	var _val = $Timer.connect("timeout", self, "enableTurretBehavior")
	for child in get_children():
		if child.is_in_group("Turret"):
			turrets.append(child)
	startTimer()

func attachParent(parent):
	actor = parent

func getTurretCount():
	return turrets.size()

func getTurret(num):
	return turrets[num]

func enableTurretBehavior():
	armed = true

func disableTurretBehavior(time):
	armed = false
	setTime(time)
	startTimer()

func turretBehavior(_delta, _target = null):
	pass

func setTime(time):
	timer_time.wait_time = time

func getTime():
	return timer_time.get_time_left()

func startTimer():
	$Timer.start()