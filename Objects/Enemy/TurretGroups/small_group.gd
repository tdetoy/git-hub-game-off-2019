extends "turret_group.gd"

const cooldown = 9.0
const volley_size = 40
const rot_speed = PI / 30
const shot_cooldown = .3

var shots_left = -10
var cur_cooldown = -10

var bullet = load("res://Objects/Enemy/TurretGroups/Turret/Bullet/Bullet.tscn")

func turretBehavior(_delta, _target = null):
	if !armed:
		for turret in turrets:
			turret.rotation = range_lerp(getTime(), cooldown, 0.0, turret.resting_rotation - PI, turret.resting_rotation)
		return
	elif armed:
		if shots_left == -10:
			shots_left = volley_size
			cur_cooldown = shot_cooldown
		if shots_left > 0:
			cur_cooldown -= _delta
			if cur_cooldown <= 0.0:
				cur_cooldown = shot_cooldown
				for turret in turrets:
					var bullet_inst = bullet.instance()
					bullet_inst.position = actor.getBulletSpawn().to_local(turret.getNextBarrel())
					bullet_inst.rotation = turret.rotation + get_parent().rotation
					actor.getBulletSpawn().add_child(bullet_inst)
					if SceneHandler.sound_on:
						$AudioStreamPlayer2D.play()
				shots_left -= 1
			for turret in turrets:
				turret.rotation = range_lerp(shot_cooldown * shots_left + cur_cooldown, shot_cooldown * volley_size, 0.0, turret.resting_rotation, turret.resting_rotation - PI)
		else:
			disableTurretBehavior(cooldown)
			shots_left = -10
			cur_cooldown = -10