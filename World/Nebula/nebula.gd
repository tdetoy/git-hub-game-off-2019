extends Node2D

var nebula_colors = [Color(), Color()]

func _ready():
	randomize()
	
	nebula_colors = determineNebulaColors(nebula_colors, 0.5, 0.7, 0.6, 0.8)

	VisualServer.set_default_clear_color(Color(nebula_colors[1].r - .5, nebula_colors[1].g - .5, nebula_colors[1].b - .5))
	
	$Nebula.texture.noise.seed = randi()
	setColors($Nebula, nebula_colors[0])

func setColors(node, color):
	node.material.set_shader_param("r", color.r)
	node.material.set_shader_param("g", color.g)
	node.material.set_shader_param("b", color.b)

func determineNebulaColors(array, low1, high1, low2, high2):
	var color1 = randi() % 3
	var color2 = randi() % 3
	
	match color1:
		0:
			array[0] = Color(rand_range(low1, high1), 0.0, 0.0)
		1:
			array[0] = Color(0.0, rand_range(low1, high1), 0.0)
		2:
			array[0] = Color(0.0, 0.0, rand_range(low1, high1))
	if color1 != color2:
		match color2:
			0:
				array[0].r = rand_range(low1, high1)
			1:
				array[0].g = rand_range(low1, high1)
			2:
				array[0].b = rand_range(low1, high1)

	var flags = [0, 0, 0]
	if array[0].r != 0.0:
		flags[0] = 1
	if array[0].g != 0.0:
		flags[1] = 1
	if array[0].b != 0.0:
		flags[2] = 1
	
	if flags[0] == 0:
		array[1] = Color(rand_range(low2, high2), 0.0, 0.0)
		if randi() % 2 == 0:
			match randi() % 2:
				0:
					array[1].g = rand_range(low2, high2)
				1:
					array[1].b = rand_range(low2, high2)
	elif flags[1] == 0:
		array[1] = Color(0.0, rand_range(low2, high2), 0.0)
		if randi() % 2 == 0:
			match randi() % 2:
				0:
					array[1].r = rand_range(low2, high2)
				1:
					array[1].b = rand_range(low2, high2)
	elif flags[2] == 0:
		array[1] = Color(0.0, 0.0, rand_range(low2, high2))
		if randi() % 2 == 0:
			match randi() % 2:
				0:
					array[1].r = rand_range(low2, high2)
				1:
					array[1].g = rand_range(low2, high2)
	
	return array