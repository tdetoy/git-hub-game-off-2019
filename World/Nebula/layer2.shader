shader_type canvas_item;

uniform float r = 0.4;
uniform float g = 0.4;
uniform float b = 0.0;


float map(float x, float in_min, float in_max, float out_min, float out_max){
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void fragment() {
	float add = COLOR.r + COLOR.g + COLOR.b;

	COLOR = texture(TEXTURE,UV);
	COLOR.a = map(COLOR.r + COLOR.g + COLOR.b, 0.0, 2.0, 0.0, 1.0);
	COLOR.r = r;
	COLOR.g = g;
	COLOR.b = b;
}
