extends Node

#Main Menu
#Options
#Game
#Pause Screen
#Postgame Report

onready var music_player = $Music

var current_scene

var pause_scene

var sound_on = true
var music_on = true

var music = {
	"Main Menu":"res://World/Abstract/Juhani Junkala [Retro Game Music Pack] Ending.wav",
	"Game":"res://World/Abstract/Juhani Junkala [Retro Game Music Pack] Level 2.wav",
	"Last Stand":"res://World/Abstract/CrazyLevel.wav"
}

var scenes = {
	"Main Menu":"res://World/Abstract/UI/Menus/MainMenu.tscn",
	"Game":"res://World/Map/Map.tscn",
	"Postgame":"res://World/Abstract/UI/Menus/PostGame.tscn",
	"Options1": "res://World/Abstract/UI/Menus/Options1.tscn",
	"Options2":"res://World/Abstract/UI/Menus/Options2.tscn",
	"Credits":"res://World/Abstract/UI/Menus/Credits.tscn"
}

func _ready():
	set_process(true)

func _process(delta):
	if !music_player.is_playing() and music_on:
		music_player.play()
	elif music_player.is_playing() and !music_on:
		music_player.stop()

func transition(scene, _args = []):
	$Tween.interpolate_property($CanvasModulate, "color", Color(1.0,1.0,1.0,1.0), Color(0.0,0.0,0.0,0.0), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	yield($Tween, "tween_all_completed")
	var new_scene = load(scenes[scene])
	var scene_instance = new_scene.instance()
	current_scene.queue_free()
	get_tree().get_root().add_child(scene_instance)
	current_scene = scene_instance
	if _args.size() > 0:
		scene_instance.initialize(_args)
	$Tween.interpolate_property($CanvasModulate, "color", Color(0.0,0.0,0.0,0.0), Color(1.0,1.0,1.0,1.0), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	yield($Tween, "tween_all_completed")

func setCurrentScene(scene):
	current_scene = scene
	changeMusic("Main Menu")

func pause():
	current_scene.lockMouse(false)
	get_tree().paused = true
#	current_scene._UI.visible = false
	pause_scene = load("res://World/Abstract/UI/Menus/Pause.tscn").instance()
	
	$Tween.interpolate_property($CanvasModulate, "color", Color(1.0,1.0,1.0,1.0), Color(0.0,0.0,0.0,0.5), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	yield($Tween, "tween_all_completed")
	get_tree().get_root().add_child(pause_scene)

func unpause():
	current_scene.lockMouse(true)
	
	$Tween.interpolate_property($CanvasModulate, "color", Color(0.0,0.0,0.0,0.5), Color(1.0,1.0,1.0,1.0), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	yield($Tween, "tween_all_completed")
	
#	current_scene._UI.visible = true
	get_tree().paused = false
	pause_scene.queue_free()

func returnToMain():
	pause_scene.queue_free()
	get_tree().paused = false
	transition("Main Menu")

func changeMusic(music_title):
	if music_on:
		music_player.stop()
		music_player.stream = load(music[music_title])
		music_player.play(0)

func buttonNoise():
	if sound_on:
		$Button.play()

func toggleSound(value):
	sound_on = !value

func toggleMusic(value):
	music_on = !value