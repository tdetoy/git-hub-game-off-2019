extends VBoxContainer

const num_flashes = 3
const flip_time = 0.5

var active_bit = false
var flash_num = 0
var time_elapsed = 0.0

func _ready():
	$Escapees.value = 0.0
	set_process(true)

func _process(delta):
	if active_bit:
		if flash_num > num_flashes:
			active_bit = !active_bit
			return
		time_elapsed -= delta
		if time_elapsed <= 0.0:
			if $Label.modulate.a == 1.0:
				$Label.modulate.a = 0.0
				flash_num += 1
			else:
				$Label.modulate.a = 1.0
	$Escapees.tint_progress.r = range_lerp($Escapees.value, 0, 100, 0.0, 1.0)
	$Escapees.tint_progress.g = range_lerp($Escapees.value, 0, 100, 1.0, 0.0)

func shipEscaped():
	$Label.modulate.a = 1.0
	$Escapees.value += 34
	active_bit = true
	flash_num = 0
	time_elapsed = flip_time

func getDanger():
	return $Escapees.value