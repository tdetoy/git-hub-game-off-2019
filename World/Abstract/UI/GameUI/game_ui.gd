extends CanvasLayer

onready var jump_meter = $MarginContainer/VSplitContainer/CenterContainer/ProgressBar
onready var oob_warning = $MarginContainer/VSplitContainer/VBoxContainer/MarginContainer/MarginContainer/Alerts
onready var ship_health = $MarginContainer/VSplitContainer/VBoxContainer/HBoxContainer/Health
onready var snafu_o_meter = $MarginContainer/VSplitContainer/VBoxContainer/HBoxContainer/VBoxContainer
onready var score_info = $"MarginContainer/VSplitContainer/VBoxContainer/HBoxContainer/Deadzone - Keep Open For Player"

func _ready():
	jump_meter.value = 0
	oob_warning.visible = false
	ship_health.value = 100
	set_process(true)

func updateHealth(value):
	ship_health.updateH(value)

func activateOOB():
	oob_warning.activate()

func deactivateOOB():
	oob_warning.deactivate()

func OOBActivated():
	return oob_warning.activated

func shipEscaped():
	snafu_o_meter.shipEscaped()

func getDanger():
	return snafu_o_meter.getDanger()

func scoreIncrease():
	score_info.scoreIncrease()

func getScore():
	return score_info.score_val

func lastStand(value):
	if value:
		score_info.lastStandActivate()
	elif !value:
		score_info.lastStandDeactivate()

func isLastStand():
	return score_info.activated