extends CenterContainer

var descending_alpha = true
var activated = false

var score_val = 0

onready var score = $VBoxContainer/Score
onready var last_stand = $VBoxContainer/LastStand

func _ready():
	score.text = "Eliminated: " + str(score_val)
	set_process(true)

func _process(delta):
	if activated:
		if descending_alpha:
			last_stand.modulate.a -= .1
			if last_stand.modulate.a <= 0.0:
				descending_alpha = false
		elif !descending_alpha:
			last_stand.modulate.a += .1
			if last_stand.modulate.a >= 1.0:
				descending_alpha = true

func lastStandActivate():
	last_stand.modulate.a = 1.0
	activated = true

func lastStandDeactivate():
	last_stand.modulate.a = 0.0
	activated = false

func scoreIncrease():
	if !activated:
		score_val += 1
	elif activated:
		score_val += 3
	score.text = "Eliminated: " + str(score_val)