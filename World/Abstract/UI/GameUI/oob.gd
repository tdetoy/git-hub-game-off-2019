extends TextEdit

var descending_alpha = true
var activated = false

func _ready():
	set_process(true)

func _process(delta):
	if activated:
		if descending_alpha:
			modulate.a -= .1
			if modulate.a <= 0.0:
				descending_alpha = false
		elif !descending_alpha:
			modulate.a += .1
			if modulate.a >= 1.0:
				descending_alpha = true

func activate():
	visible = true
	activated = true

func deactivate():
	visible = false
	activated = false