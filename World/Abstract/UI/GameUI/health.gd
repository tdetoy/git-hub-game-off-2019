extends TextureProgress

signal low_health

const blink_interval = .75
const blink_threshold = 20

var blink_period
var blink_bit = false

func _ready():
	set_process(true)
	blink_period = blink_interval

func _process(delta):
	if blink_bit:
		blink_period -= delta
		if blink_period <= 0.0:
			if tint_progress.a == 0.0:
				tint_progress.a = .8
				tint_under.a = .2
			else:
				tint_progress.a = 0.0
				tint_under.a = .2
			blink_period = blink_interval

func updateH(health):
	tint_progress.g = range_lerp(value, 100, 20, 1.0, 0.0)
	tint_progress.r = range_lerp(value, 100, 20, 0.0, 1.0)
	value = health * 100
	if (value <= blink_threshold):
		emit_signal("low_health")
		blink_bit =  true