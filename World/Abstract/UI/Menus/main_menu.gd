extends Control

signal button

func _ready():
	connect("button", SceneHandler, "buttonNoise")
	SceneHandler.setCurrentScene(self)
	$MarginContainer/VSplitContainer/HBoxContainer/CenterContainer2/VBoxContainer/HBoxContainer/Sound.pressed = !SceneHandler.sound_on
	$MarginContainer/VSplitContainer/HBoxContainer/CenterContainer2/VBoxContainer/HBoxContainer/Music.pressed = !SceneHandler.music_on
	set_process(true)

func _process(delta):
	$NebulaBackground.position.x += delta * 10

func _on_Start_pressed():
	emit_signal("button")
	SceneHandler.transition("Game")
	SceneHandler.changeMusic("Game")


func _on_Options_pressed():
	emit_signal("button")
	SceneHandler.transition("Options1")


func _on_Exit_pressed():
	emit_signal("button")
	get_tree().quit()


func _on_Sound_toggled(button_pressed):
	SceneHandler.toggleSound(button_pressed)


func _on_Music_toggled(button_pressed):
	SceneHandler.toggleMusic(button_pressed)
