extends Control

func _ready():
	connect("button", SceneHandler, "buttonNoise")

func _on_Exit_pressed():
	emit_signal("button")
	SceneHandler.transition("Main Menu")


func _on_BasicControls_pressed():
	emit_signal("button")
	SceneHandler.transition("Options1")
