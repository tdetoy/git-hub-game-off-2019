extends CanvasLayer

signal button

func initialize(_args = []):
	connect("button", SceneHandler, "buttonNoise")
	print("into post initialize")
	print(Input.get_mouse_mode())
	var amount = _args[0]
	$MarginContainer/CenterContainer/VBoxContainer/CenterContainer2/VBoxContainer/Recap.text = "You heroically prevented\n" + str(amount) + "\ncommonwealth frigates from continuing their march into your territory"
	if amount < 10:
		$MarginContainer/CenterContainer/VBoxContainer/CenterContainer2/VBoxContainer/Flavor.text = "Your sacrifice is barely remembered"
	elif amount >= 10 && amount < 20:
		$MarginContainer/CenterContainer/VBoxContainer/CenterContainer2/VBoxContainer/Flavor.text = "You earned the commonweath's respect and fear"
	else:
		$MarginContainer/CenterContainer/VBoxContainer/CenterContainer2/VBoxContainer/Flavor.text = "Your name will go down in song and legend"

func _on_Retry_pressed():
	emit_signal("button")
	SceneHandler.transition("Game")
	SceneHandler.changeMusic("Game")


func _on_Exit_pressed():
	emit_signal("button")
	SceneHandler.transition("Main Menu")
