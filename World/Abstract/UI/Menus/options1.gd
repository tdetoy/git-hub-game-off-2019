extends Control


func _ready():
	connect("button", SceneHandler, "buttonNoise")
	VisualServer.set_default_clear_color(Color(0.0,0.0,0.0))

func _on_Credits_pressed():
	emit_signal("button")
	SceneHandler.transition("Credits")


func _on_Exit_pressed():
	emit_signal("button")
	SceneHandler.transition("Main Menu")


func _on_WarpControls_pressed():
	emit_signal("button")
	SceneHandler.transition("Options2")
