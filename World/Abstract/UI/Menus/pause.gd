extends CanvasLayer

signal button

func _ready():
	connect("button", SceneHandler, "buttonNoise")

func _on_Resume_pressed():
	emit_signal("button")
	SceneHandler.unpause()


func _on_Exit_pressed():
	emit_signal("button")
	SceneHandler.changeMusic("Main Menu")
	SceneHandler.returnToMain()
