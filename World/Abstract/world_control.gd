extends Resource

const max_magnitude = 20000
const max_cooldown_limit = 3.0
const min_cooldown_limit = 2.0

var max_enemies = 20

var magnitude = 5000
var max_cooldown = 7.0
var min_cooldown = 5.0
var cooldown_reduce_bit = false
var last_stand = false

var cooldown
var map


func init(map_in):
	randomize()
	cooldown = rand_range(min_cooldown, max_cooldown)
	max_cooldown -= 0.1
	map = map_in

func update(delta):
	cooldown -= delta
	if cooldown <= 0.0 and map.enemy_list.size() < max_enemies:
		map.spawnEnemy(magnitude)
		magnitude *= 1.1
		magnitude = min(magnitude, max_magnitude)
		if !cooldown_reduce_bit:
			min_cooldown = max(min_cooldown_limit, min_cooldown - 0.3)
		elif cooldown_reduce_bit:
			max_cooldown = max(max_cooldown_limit, max_cooldown - 0.3)
		cooldown_reduce_bit = !cooldown_reduce_bit
		if map._UI.getDanger() >= 100:
			if !map._UI.isLastStand():
				map.activateLastStand()
				last_stand = true
				map._UI.lastStand(true)
			cooldown = 0.5
		else:
			cooldown = rand_range(min_cooldown, max_cooldown)