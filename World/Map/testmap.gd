extends Node2D

var enemy_scene = load("res://Objects/Enemy/Enemy.tscn")
var engine_trail = load("res://Objects/Enemy/TurretGroups/Turret/Laser/Laser.tscn")

var world_control
var enemy_list = {}
var engine_trails = {}
var mouse_locked = true

onready var _UI = $GameUI
onready var player = $Player

func _ready():
	randomize()
	_UI.ship_health.connect("low_health", player, "lowHealth")
	lockMouse(true)
	world_control = load("res://World/Abstract/world_control.gd").new()
	world_control.init(self)
	$Player.attachMap(self)
	var enemy = load("res://Objects/Enemy/Enemy.tscn").instance()
	enemy_list[enemy.name] = weakref(enemy)
	add_child(enemy)
	enemy.color = $NebulaBackground.nebula_colors[1]
	enemy.setInit(Vector2(350,1650), 0.0, Vector2(10000,10000), self)
	registerEngineTrail(enemy, Color(0.8,0.0,0.0,1.0))
	set_process(true)

func registerEngineTrail(actor, color):
	engine_trails[actor.name] = engine_trail.instance()
	$EngineTrailsParent.add_child(engine_trails[actor.name])
	engine_trails[actor.name].setLaser(false, actor)
	engine_trails[actor.name].setColor(color)
	if actor.is_in_group("Enemy"):
		engine_trails[actor.name].setLongTrail(true)

func deregisterEngineTrail(actor):
	var trail = engine_trails[actor.name]
	engine_trails.erase(actor.name)
	trail.queue_free()

#clamp the mouse x position within certain constraints on the screen
#keep its y position in the same place
func _process(delta):
	if Input.is_action_pressed("ui_cancel"):
		SceneHandler.pause()
	if mouse_locked:
		var x_pos = max(player.camera.get_viewport().size.x / 3, player.camera.get_viewport().get_mouse_position().x)
		x_pos = min(x_pos, 2 * player.camera.get_viewport().size.x / 3)
		player.camera.get_viewport().warp_mouse(Vector2(x_pos, player.camera.get_viewport().size.y / 5))
	world_control.update(delta)

func _draw():
	var i = -30000
	while i < 30001:
		draw_line(Vector2(i, -30000), Vector2(i, 30000), Color.white, 5.0)
		i += 250
	var j = -30000
	while j < 30001:
		draw_line(Vector2(-30000, j), Vector2(30000, j), Color.white, 5.0)
		j += 250

#instance a new enemy, set a random position and rotation, and then just fuckn
#shove em in a corner until they're ready to spawn in. just... just hide em in
#the corner.
func spawnEnemy(magnitude):
	var rot_value = rand_range(0.0, 2 * PI - 1)
	var spawn_pos
	if !world_control.last_stand:
		spawn_pos = Vector2(0,-1).rotated(rot_value)
		spawn_pos *= magnitude
		magnitude *= 1.1
	else:
		spawn_pos = $Player.position.rotated(rot_value)
		spawn_pos.x += rand_range(-5000, 5000)
		spawn_pos.y += rand_range(-5000, 5000)
	magnitude = min(magnitude, 20000)
	var enemy = enemy_scene.instance()
	add_child(enemy)
	enemy_list[enemy.name] = weakref(enemy)
	enemy.color = $NebulaBackground.nebula_colors[1]
	enemy.setInit(spawn_pos, rand_range(0.0, 2 * PI - 1), Vector2(100000 + (enemy_list.size() * 1000), 100000 + (enemy_list.size() * 1000)), self)
	registerEngineTrail(enemy, enemy.color)
#	for ship in enemy_list.keys():
#		if !enemy_list[ship].get_ref():
#			enemy_list.erase(ship)

func getEnemyList():
	return enemy_list

func getBulletParent():
	return $BulletParent

func getExplosionParent():
	return $ExplosionParent

func getSpawnPoint():
	return $EnemyParent

func getPlayer():
	return $Player

func addEngineTrailPoint(actor, pos):
	engine_trails[actor.name].addTrackPoint(to_local(actor.engine_trail.global_position))

func onEntityDeath(actor):
	if actor.is_in_group("Player"):
		lockMouse(false)
		SceneHandler.transition("Postgame", [_UI.getScore()])
		SceneHandler.changeMusic("Main Menu")
	else:
		deregisterEngineTrail(actor)
		enemy_list.erase(actor.name)

func resetPlayerTrail(player):
	engine_trails[player.name].clearPoints()

func activateLastStand():
	world_control.max_enemies = int(float(world_control.max_enemies) * 1.5)
	SceneHandler.changeMusic("Last Stand")

func lockMouse(value):
	if value:
		mouse_locked = true
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	elif !value:
		mouse_locked = false
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		print(Input.get_mouse_mode())